'''
driver assignment 2
Name:  
'''

from mydsp.audioframes import AudioFrames
from mydsp.rmsstream import RMSStream
from mydsp import plots
import matplotlib.pyplot as plt
import numpy as np
from mydsp.multifileaudioframes import MultiFileAudioFrames
from mydsp.dftstream import DFTStream
from mydsp.utils import get_corpus
from mydsp.pca import PCA
import os
import sklearn.mixture as mixture

# Write any helper functions you need here

    

def plot_narrowband_wideband(filename):
    """plot_narrowband_widband(filename)
    Given a speech file, display narrowband and wideband
    spectrograms of the speech.
    """
    # Compute wide band spectrogram for given file with frame adv_ms = 10 and len_ms = 20
    files = [filename]
    wide_magnitude, wide_t_axis, wide_f_axis = plots.spectrogram (files,10, 20)
    wide_magnitude_trans = np.transpose(wide_magnitude)

    #Wide band spectrogram plot
    fig = plt.figure()
    plt.subplot(211)
    plt.pcolormesh(wide_t_axis, wide_f_axis, wide_magnitude_trans)
    plt.title("Wide band Spectrogram")
    plt.xlabel('time in sec')
    plt.ylabel('freq. in Hz')
    fig.text(.5, 0.53, "Wide band has good time resolution, but poor frequency resolution", ha='center')
    plt.colorbar()

    # Narrow band spectrogram for given file with frame_adv = 20 and len_ms = 40
    narrow_magnitude, narrow_t_axis, narrow_f_axis = plots.spectrogram(files, 20, 40)
    narrow_magnitude_trans = np.transpose(narrow_magnitude)

    #Narrow band spectrogram plot
    plt.subplot(212)
    plt.pcolormesh(narrow_t_axis, narrow_f_axis, narrow_magnitude_trans)
    plt.title("Narrow band Spectrogram")
    plt.xlabel('time in sec')
    plt.ylabel('freq. in Hz')
    fig.text(.5, 0.05, "Narrow band has good frequency resolution, but poor time resolution", ha='center')
    plt.colorbar()
    plt.tight_layout()
    plt.show()
    print()

def pca_analysis(corpus_dir):
    """pca_analysis(corpus_dir)
    Given a directory containing a speech corpus, compute spectral features
    and conduct a PCA analysis of the data.
    """
    # 3A
    # computing spectrogram data for multifiles data
    frames = MultiFileAudioFrames(corpus_dir,10,20)
    dft = DFTStream(frames)

    # list of dft stream
    dft_frames = dft.get_dft_frames()
    pca = PCA(dft_frames)

    # computing variance captured by m components
    variance_kept = (np.cumsum(pca.eigen_value) / np.sum(pca.eigen_value))

    # plotting the graph for captured variance against no of components
    fig = plt.figure()
    plt.plot(variance_kept)
    plt.xlabel('No. of components')
    plt.ylabel('Variance Captured')
    fig.text(.5, 0.02, "Variance captured per Number of components", ha='center')
    plt.show()

    # 3B
    variance_kept = np.abs(sorted(variance_kept))

    # computing number of components needed for each decile of variance
    number_components = get_no_of_comp_for_var(variance_kept)

    # 3C
    # Create .6a file
    file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "woman/ac/6a.wav")

    # New instances for DFTStream and MultiFileAudioFrames
    frames = MultiFileAudioFrames([file_path],10,20)
    dft = DFTStream(frames)
    dft_frames = dft.get_dft_frames()

    # Transform of single wav file (60% variance)
    transform_data = pca.transform(dft_frames,number_components[5])

    # Compute time axis
    step = 10 * len(dft_frames)
    time_A = []
    for itr in range(0, step, 10):
        time_A.append(itr/1000)
    time_A = np.array(time_A)

    # Compute absolute value for PCA transform
    pca_transform = np.transpose(transform_data)
    pca_magnitude = np.abs(pca_transform)

    # Compute PCA axis in place of freq axis for plotting spectrogram
    pca_A = []
    for itr in range(0,len(pca_magnitude)):
        pca_A.append(itr)
    pca_A = np.array(pca_A)

    # Spectrogram for time vs PCA component
    fig = plt.figure()
    plt.pcolormesh(time_A, pca_A, pca_magnitude)
    plt.title("Spectrogram for PCA vs time")
    plt.xlabel('Seconds')
    plt.ylabel('PCA Components')
    plt.colorbar()
    fig.text(0.5,0.01,"New values in PCA space",ha='center')
    plt.show()



def get_no_of_comp_for_var(variance_kept):
    '''

    For computing number of components to capture deciles of variance and printing the results.
    :param variance_kept: Variance captured until ith component
    :return: number of components required for each decile of variance captured
    '''
    decile = 0.1
    var_count=1
    num_com = []
    for i in variance_kept:
        while i >= decile:
            num_com.append(var_count)
            print("Number of components to cover "+ str(len(num_com)*10) + "% variance is : "+str(var_count))
            decile+=0.1
            continue
        var_count += 1
        if(decile>=1.0):
            break
    return num_com

  

def speech_silence(filename):
    """speech_silence(filename)
    Given speech file filename, train a 2 mixture GMM on the
    RMS intensity and label each frame as speech or silence.
    Provides a plot of results.
    """
    audio_frames = AudioFrames(filename=filename, adv_ms=10, len_ms=20)
    rms_streams = RMSStream(audio_frames)

    #Store rms frames in ndarray to pass in model parameters
    streams = np.asarray(rms_streams.get_rms_frames())

    input = np.reshape(streams, (-1, 1))

    model = mixture.GaussianMixture(n_components=2)
    model.fit(input)
    labels = model.predict(input)

    #Generate time axis values
    time_axis = create_time_axis(rms_streams.frame_stream,audio_frames)

    # separate speech and noise labels to plot separately
    [input_noise,input_speech,time_speech,time_noise] = divide_by_labels(labels,input,time_axis,model)

    #Plot graph between RMS intensity and time with speech/noise distribution
    plt.ion()
    fig = plt.figure()
    plt.scatter(time_speech, input_speech, marker='x',label='Speech')
    plt.scatter(time_noise, input_noise, marker='o',label='Noise')
    plt.title("RMS Intensity vs Time with Speech/Noise distribution")
    plt.xlabel("time in sec")
    plt.ylabel('dB')
    plt.legend()
    fig.text(0.5,0.03,"Distribution of speech and noise data",ha = 'center')
    plt.show()
    print()



def create_time_axis(rms_stream,audio_frames):
    """
    Accepts rms_stream data and audio frames data to generate time axis values to plot RMS intensity graph.
    """
    counter = 0
    start = 0
    time_axis = []
    while counter < len(rms_stream):
        time_axis.append(start)
        start += audio_frames.get_frameadv_ms() / 1000
        counter += 1
    return time_axis



def divide_by_labels(labels,input,time_axis,model):
    """
    Divides the generated labels of model into 2 separate array of speech and noise, so that they can be plotted with different markers.
    """
    input_noise = []
    input_speech = []
    time_speech = []
    time_noise = []

    #Higher mean distribution value will be the mean in our case.
    [mean_x,mean_y] = model.means_
    if mean_x>mean_y:
        speech_label = 0
    else:
        speech_label = 1

    i = 0

    #assign input values and time values to different lists according to the label.
    while i < labels.size:
        if labels[i] == speech_label:
            input_speech.append(input[i])
            time_speech.append(time_axis[i])
        else:
            input_noise.append(input[i])
            time_noise.append(time_axis[i])
        i += 1
    np.asarray(input_noise)
    np.asarray(input_speech)

    return [input_noise,input_speech,time_speech,time_noise]

    
if __name__ == '__main__':
    # If we are here, we are in the script-level environment; that is
    # the user has invoked python driver.py.  The module name of the top
    # level script is always __main__

    plot_narrowband_wideband("/Users/nakulnarwaria/PycharmProjects/SpeechProcessingAssignment/shaken.wav")
    speech_silence("/Users/nakulnarwaria/PycharmProjects/SpeechProcessingAssignment/shaken.wav")
    dir = get_corpus("/Users/nakulnarwaria/PycharmProjects/SpeechProcessingAssignment/woman")
    pca_analysis(dir)

    pass # for breakpoint if desired
    
    