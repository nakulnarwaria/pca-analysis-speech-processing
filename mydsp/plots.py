    
from .multifileaudioframes import  MultiFileAudioFrames
import matplotlib.pyplot as plt
from scipy import signal
from mydsp import dftstream
from mydsp import audioframes
import numpy as np
import os,sys



def spectrogram(files, adv_ms, len_ms, plot=False):
    """spectrogram(files, adv_ms, len_ms, plot)
    Given a list of files and framing parameters (advance, length in ms), 
    compute a spectrogram that spans the files.  
    
    Returns [intensity_dB, taxis, faxis]
    
    If optional plot is True (default False), the spectrogram is plotted
    """

    # computes and stores frames across multiple files
    frames = MultiFileAudioFrames(files, adv_ms, len_ms)
    # get the dftstream instance using frames as param
    dft = dftstream.DFTStream(frames)

    time_axis = []
    start_time = 0

    # fetch dft mag values in the form of ndarray
    dft_mag = np.array(dft.get_dft_frames())

    # generate time axis values
    for i in range(0,len(dft_mag)):
        time_axis.append(start_time)
        start_time += (adv_ms / 1000)

    time_axis = np.array(time_axis)
    frequency_axis = dft.get_Hz()

    if plot:
        fig = plt.figure()
        plt.pcolormesh(time_axis, frequency_axis, np.transpose(dft_mag))
        plt.title("Spectogram")
        plt.xlabel('time in sec')
        plt.ylabel('freq. in Hz')
        fig.text(.5, 0.02, "Spectogram having window length of "+ str(len_ms)+"ms, and frame advance "+ str(adv_ms)+" ms", ha='center')
        plt.colorbar()

    return [dft_mag, time_axis, frequency_axis]
