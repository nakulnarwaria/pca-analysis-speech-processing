'''
dftstream - Streamer for Fourier transformed spectra
'''

import numpy as np
import scipy.signal as signal
    
     
class DFTStream:
    '''
    DFTStream - Transform a frame stream to various forms of spectra
    '''


    def __init__(self, frame_stream, specfmt="dB"):
        '''
        DFTStream(frame_stream, specfmt)        
        Create a stream of discrete Fourier transform (DFT) frames using the
        specified sample frame stream. Only bins up to the Nyquist rate are
        returned in the stream Optional arguments:
        
        specfmt - DFT output:  
            "complex" - return complex DFT results
             "dB" [default] - return power spectrum 20log10(magnitude)
             "mag^2" - magnitude squared spectrum
        '''

        self.frame_stream = frame_stream
        # Storing iterator for iterating over frame_stream in next function for this class
        self.frame_stream_itr = iter(frame_stream)
        self.specfmt = specfmt


         
    def shape(self):
        "shape() - Return dimensions of tensor yielded by next()"
        hz = self.get_Hz()
        return np.asarray([len(hz), 1])
    
    def size(self):
        "size() - number of elements in tensor generated by iterator"
        return np.asarray(np.product(self.shape()))
   
    def get_Hz(self):
        """get_Hz() - Return list of frequencies associated with each 
        spectral bin.  (List is of the same size as the # of spectral
        bins up to the Nyquist rate, or half the frame lenght)
        """
        #compute total samples given sampling rate and frame length in ms
        total_samples = int(self.frame_stream.Fs * (self.frame_stream.len_ms / 1000.0))

        # Nyquist rate is half the sampling rate
        self.nq_freq = self.frame_stream.Fs / 2

        # Compute frequencies associated with each spectral bin
        self.freq_hz = (np.arange(total_samples)) * (self.nq_freq / total_samples)

        return self.freq_hz

            
    def __iter__(self):
        "iter() Return iterator for stream"
        return self
    
    def __next__(self):
        """
        Computes the DFT of current frame using hamming window and returns it
        """
        frame = np.array(next(self.frame_stream_itr))
        hamming_window = signal.get_window("hamming", frame.shape[0])

        # extract signal part
        windowed_frames = frame * hamming_window

        # Compute dft of windowed signal
        dft = np.fft.fft(windowed_frames)
        abs = np.abs(dft)

        # Handle the specfmt if needed
        if self.specfmt == "dB":
            dft= 20 * np.log10(abs)

        if self.specfmt == "mag^2":
            dft= 10 * np.log10(abs)

        return dft
        
    def __len__(self):
        "len() - Number of tensors in stream"
        return len(self.frame_stream)

    def get_dft_frames(self):
        """
        Returns dft stream as a list
        """
        dft_stream = []
        for i in self:
            dft_stream.append(i)
        return dft_stream

        
        
        
    
        