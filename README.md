## Abstract
---

This project is an extension of the first problem set, Audio-RMS-Energy-Plot, and now Spectrograms( both narrowband and wideband) and PCA Analyis of the audi data is to be obtained. Following implementations are coded in the project - 

A class mydsp.DFTStream is added which like RMSStream, takes an instance of a FrameStream object in its constructor. Another function in module mydsp.plots called spectrogram is implemented that given a list of files, and frame parameters, it creates a spectrogram across all of the files. Module mydsp.multifileaudiostream contains a class that will let you create a frame stream across multiple files.

The mydsp.pca module is responsible for PCA Analysis of the Audio data, provided in the form of directory to read Audio data from. The mydsp.utils module contains function get_corpus. Given a root directory, it will find all filenames contained in that directory or its children with a specified extension (default .wav).

A function named speech_silence is also included in driver module that takes afilename as an argument. The function will create an RMSStream from the file
(20 ms frame length, 10 ms advance) and compute the RMS energy for each frame in the file. 

## Sample Use: 
---

In the driver module attached in this project, there is a function plot_narrowband_wideband that reads the audio file from a specified file and plots its spectrogram twice, once as a narrow-band spectrogram, and again as a wide-band spectrogram. This function is called from the top-level script environment with the shaken.wav file(this is the part in the “if __name__ == ‘__main__:’” block). Sample plots are shown below. 
A subset of the female training data in the TIDIGITS corpus of spoken digits is available with this project. A plot of the number of PCA components used versus the amount of variance captured is shown with this data.

![Wide_band_Narrow_band_spectrogram](https://user-images.githubusercontent.com/12014727/54397147-a8940600-4672-11e9-86af-e1d8c2324b7a.png)

![variance_captured](https://user-images.githubusercontent.com/12014727/54397171-c6616b00-4672-11e9-821d-76704ec78747.png)

![PCA_Space](https://user-images.githubusercontent.com/12014727/54397185-d24d2d00-4672-11e9-8ee1-753a4bd6dc0f.png)

![speech_silence_distribution](https://user-images.githubusercontent.com/12014727/54397195-daa56800-4672-11e9-9d75-e82a4759c8e8.png)

## Credits
---

**Professor Marie A. Roch** (San Diego State University)


